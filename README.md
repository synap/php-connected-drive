# PHPConnectedDrive

A PHP cronjob to fetch data from BMW Connected Drive.
Inspired by https://github.com/sergejmueller/battery.ebiene.de and implemented with Symfony components

## How to use


    git clone https://gitlab.com/synap/php-connected-drive.git
    cd php-connected-drive
    composer install

Then copy .env.dist in .env and update values

Then in your crontab :

    php cron.php >> events.csv
