<?php

require_once 'vendor/autoload.php';

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Component\HttpClient\Exception\RedirectionException;
use Symfony\Component\Dotenv\Dotenv;

// We load secrets data
$dotenv = new Dotenv();
$dotenv->load(__DIR__.'/.env');

$options = [
    'username' => $_ENV['LOGIN'],
    'password' => $_ENV['PASSW'],
    'VIN' => $_ENV['VIN'],
];

// We create a HTTP client
$client = HttpClient::create();

// If we have a valid token, we use it. Otherwise, we ask on by using credentials.
$cache = new FilesystemAdapter('cacaboudin6');

$token = $cache->get('token', function (ItemInterface $item) use($client, $options) {

    $credentials = array_intersect_key($options, array_flip(['username', 'password']));

    $response = $client->request('POST','https://customer.bmwgroup.com/gcdm/oauth/authenticate', [
        'max_redirects' => 0,
        'body' => [
            'client_id' => 'dbf0a542-ebd1-4ff0-a9a7-55172fbfce35',
            'redirect_uri' => 'https://www.bmw-connecteddrive.com/app/default/static/external-dispatch.html',
            'response_type' => 'token',
            'scope' => 'authenticate_user fupo',
            'state' => 'eyJtYXJrZXQiOiJkZSIsImxhbmd1YWdlIjoiZGUiLCJkZXN0aW5hdGlvbiI6ImxhbmRpbmdQYWdlIn0',
            'locale' => 'FR-fr',
        ] + $credentials
    ]);

    preg_match( '/access_token=([\w\d]+).*token_type=(\w+).*expires_in=(\d+)/', $response->getHeaders($throw = false)['location'][0], $matches );
    list($all, $token, $type, $expire) = $matches;

    $item->expiresAfter((int) $expire);

    return $token;
});

// Requesting data about vehicule with provided VIN.
$response = $client->request('GET',"https://www.bmw-connecteddrive.de/api/vehicle/dynamic/v1/{$options['VIN']}?offset=-60", [
    'auth_bearer' => $token,
]);

$response = $response->getContent();

$ctag = $cache->getItem('ctag');

if ($ctag->get() == ($md5 = md5($response))) {
    exit();
}

$fp = fopen('php://stdout', 'w');

fputcsv($fp, [time(), base64_encode(gzcompress($response,9))]);

$ctag->set($md5);
$cache->save($ctag);

fclose($fp);

